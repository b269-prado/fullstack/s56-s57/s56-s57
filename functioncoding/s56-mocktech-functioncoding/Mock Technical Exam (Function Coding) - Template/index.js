function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is invalid, return undefined.
    if (letter.length !== 1) {
        return undefined;
    }

    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    letter = letter.toLowerCase();
    sentence = sentence.toLowerCase();   
    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            result++;
        }
    }
    return result;   
}
console.log(countLetter('a', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'));
console.log(countLetter('abc', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'));





function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    text = text.toLowerCase();

    // If the function finds a repeating letter, return false. Otherwise, return true.
    const letters = {};

    for (let i = 0; i < text.length; i++) {
        const letter = text[i];
        if (letters[letter]) {
            return false;
        }
        letters[letter] = true;
    }
    return true;    
}
console.log(isIsogram("abcdefghijklmnopqrstuvwxyz"));
console.log(isIsogram("aaaaaaaaaaaaaaa"));





function purchase(age, price) {
    // Return undefined for people aged below 13.
    if (age < 13) {
        return undefined;
    }

    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    if ((age >= 13 && age <= 21) || age >= 65) {
        const discountPrice = price * 0.8;
        return Math.round(discountPrice).toString();
    }

    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    else {
        return Math.round(price).toString();
    }    
}
console.log(purchase("10", "100"));
console.log(purchase("15", "100"));
console.log(purchase("25", "100"));
console.log(purchase("75", "100"));


// Find categories that has no more stocks.
// The hot categories must be unique; no repeating categories.
// The passed items array from the test are the following:
let items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];

let hotCategories = [];

function findHotCategories(items) {    
    for (let i = 0; i < items.length; i++) {
        let item = items[i];

        if (item.stocks === 0) {
            if (!hotCategories.includes(item.category)) {
                hotCategories.push(item.category)
            }
        }
    }
    return hotCategories;    
}
console.log(findHotCategories(items));
// The expected output after processing the items array is ['toiletries', 'gadgets'].
// Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.


// Find voters who voted for both candidate A and candidate B.
// The passed values from the test are the following:
let candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
let candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

let flyingVoters = [];

function findFlyingVoters(candidateA, candidateB) {    
    for (let i = 0; i < candidateA.length; i++) {
        let votersForCandidateA = candidateA[i];

        if (candidateB.includes(votersForCandidateA)) {
            flyingVoters.push(votersForCandidateA);
        }
    }    
    return flyingVoters;
}
console.log(findFlyingVoters(candidateA, candidateB));
// The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
// Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};